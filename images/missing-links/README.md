### Generating Missing Links Header Images

1. Open **`stiemannkj1.gitlab.io/images/missing-links/missing-link.odg`** in LibreOffice Draw.
2. Replace the text of the third box (the missing link) with the reqiured text (usually the title of the document).
3. Click *File* > *Export* and create a folder named after the title of the post.
4. Export the image as a **`.png`** file also named after the title of the post into the folder.
5. In the *PNG Options* window, reduce the compression slider to 1.
6. Make sure that *Interlaced* and *Save transparency* are checked.
7. Click *OK*.

### Generating Missing Links Preview Images

1. Serve the blog locally according to the **`stiemannkj1.gitlab.io/README.md`** instructions.
2. Use Shutter or Snipping Tool or any other screenshot selection tool to capture the third box (the missing link). Make sure to center on the text.
3. Save the image next to the header image with the same name but suffixed with **`-preview.png`**.
