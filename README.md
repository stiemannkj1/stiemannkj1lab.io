# <a href="http://stiemannkj1.gitlab.io/" target="_blank">Kyle's Blog</a>

This repo contains the source code for my blog: <a href="http://stiemannkj1.gitlab.io/" target="_blank">stiemannkj1.gitlab.io</a>.

To build and run locally:

```
# With ruby 3.1
gem install bundler
bundle install
bundle exec jekyll serve --watch # The blog will be accessible at localhost:4000
```

If you see any errors or want to make any improvements don't hesitate to send me a pull request, <a href="https://gitlab.com/stiemannkj1/stiemannkj1.gitlab.io/issues" target="_blank">comment, and/or write a bug report</a>.

<a class="image-link" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />The content of <a href="https://gitlab.com/stiemannkj1/stiemannkj1.gitlab.io/tree/master/_posts" class="site-title" target="_blank">each post (under the **`_posts/`** directory)</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

See <a href="https://gitlab.com/stiemannkj1/stiemannkj1.gitlab.io/blob/master/ATTRIBUTION.txt" target="_blank">`ATTRIBUTION.txt`</a> for information about the licenses of other files included in this repository.
