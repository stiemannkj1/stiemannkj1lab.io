---
layout: post
title: Mario 64's Flowing Level Design
permalink: /mario-64s-flowing-level-design/
tags: [mario-64, mario, design, game-design, video-games, video-game]
comments_issue_number: 6
---

<a href="https://en.wikipedia.org/wiki/Super_Mario_64" target="_blank">Super
Mario 64</a> is one of the earliest 3D platformers. Since the game utilizes the
third dimension, the levels are designed to be less linear than Mario's 2D
entries. Many of the larger levels have multiple goals such as winning a race,
getting all the red coins, or defeating a boss. The game rewards the player with
a star for completing these challenges. Once the player obtains 60 (of 120)
stars, they can face off against Bowser. Recently, I was replaying Mario 64 to
attempt to get all 120 stars, and I started playing the level *Tiny-Huge
Island*. Every time Mario slides down a pipe in *Tiny-Huge Island*, he changes
size to either tiny or huge. While I was playing, I noticed some particularly
clever game design when I got the fourth, fifth, and sixth stars of *Tiny-Huge
Island*.

In the challenge to get the fourth star of *Tiny-Huge Island*, the player must
find *Five Itty Bitty Secrets*. As the name suggests, there are five secrets
hidden within the island, and Mario must be huge to find them. Since the fifth
and sixth stars are concealed, this challenge is desinged to hint at their
locations. By forcing the player to be big Mario, the designers made it much
easier for the player to explore the island. Huge Mario can traverse the island
much more quickly than tiny Mario. Although there are five secrets, only two of
them show players where the next stars lie. The first secret is on a tiny
out-of-place plank jutting from the wall.

<img alt="The First Secret"
src="/images/mario-64s-flowing-level-design/mario-secret1.gif"
style="width: 500px; height: auto;" />

The second sits at the island's peak.

<img alt="The Second Secret"
src="/images/mario-64s-flowing-level-design/mario-secret2.gif"
style="width: 500px; height: auto;" />

Once the player has found all five secrets, they can progress to the fifth
challenge.

To obtain the fifth star, the player must locate *Wiggler's Red Coins*. The
player has thouroughly explored the level at this point, but they have not seen
any obvious red coins. Since there are no conspicuous places left, the design
guides the player to check the secrets they were shown in *Five Itty Bitty
Secrets*. Because the player has already discovered the secrets as huge Mario,
they will want to check the those same areas with tiny Mario instead. As small
Mario, all the secrets---except the first one---are dead ends.

<img alt="The First Secret As Small Mario"
src="/images/mario-64s-flowing-level-design/secret1-small.gif"
style="width: 500px; height: auto;" />

When the player checks out the first secret, they will find *Wiggler's Red
Coins*.

<img alt="Wriggler's Red Coins"
src="/images/mario-64s-flowing-level-design/secret1-small.jpg"
style="width: 500px; height: auto;" />

The challenge is still incomplete though. In order to finish it, the player must
collect the coins. As the player gathers the coins, they will notice
eye-catching movement in the mesh ceiling.

<img alt="Wriggler"
src="/images/mario-64s-flowing-level-design/secret2-view.jpg"
style="width: 500px; height: auto;" />

Now the player's final goal is clear.

The challenge for the sixth star is to *Make Wiggler Squirm*. Because the player
has seen the opening above the wriggler, they will know that the entrance is on
the top of the mountain. Then the player may check the peak of the island as
small Mario and find the entrance closed. The next logical step is to check the
summit as big Mario. A ground pound will open the entrance.

<img alt="Ground Pound"
src="/images/mario-64s-flowing-level-design/ground-pound.gif"
style="width: 500px; height: auto;" />

Then the player must return to the peak with tiny Mario and drop into the hole.

<img alt="The Second Secret As Small Mario"
src="/images/mario-64s-flowing-level-design/secret2-small.gif"
style="width: 500px; height: auto;" />

Finally, the player must defeat wiggler to obtain the sixth star.

These three challenges integrate brilliantly. They trasition perfectly. They
flow seemlessly. The levels guide the player but do not oversimplify the task.
Teasing the player's brain, the design encourages and rewards players for
thinking about what they see. However, the design also limits the player's
options and helps them avoid getting too off course. The design attempts to keep
the player in a <a href="https://www.youtube.com/watch?v=zFv6KAdQ5SE#t=1m9s"
target="_blank">flow state</a>, and---for me at least---it succeeded.

