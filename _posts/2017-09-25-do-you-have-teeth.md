---
layout: post
title: Do You Have Teeth?
permalink: /do-you-have-teeth/
tags: [story, poem]
comments_issue_number: 15
excerpt_separator: <!--end-excerpt-->
---


> There is a generation whose teeth are like swords and whose molars are like knives to devour the poor from the earth and the needy from among the human race. 

---Proverbs 30:14

This evening, my one-year-old son, Jules, and I took a walk around the neighborhood. While Jules was cruising around picking up sticks, he spotted a woman walking her dog in the distance.

<!--end-excerpt-->

"A bah!" he exclaimed.

"That's right, a dog," I said.

After we had meandered a little further down the road, the lady said, "Can we come say hello?"

I said sure.

Her dog was certainly happy to see us and Jules was happy to see it too. At one point, he quickly reached---whether to pinch or to pet I do not know---towards the dog's snout. I speedily pushed his hand away and said, "Don't pinch."

"She'd never bite." said the owner.

I've heard this sentiment a few times, and I've never trusted it. "She won't bite." gives me no consolation. Why? Because *she has teeth*. Things with teeth are designed to rip and to tear and to bite. It's not a good idea to antagonize a thing with teeth, no matter how good-natured. So I distrust people when they take their dog's teeth lightly---when they act like their canine is all gums.

This brings me to question myself: do I have teeth? Would I bite? Yes, I have bitten for good and often for ill. I have torn at my friend's souls, and I have ripped my brother's sin. I have scratched my wife's heart and I have shredded her enemies. I have teeth.

You should have known that I have teeth.

Do you have teeth?

If you say "No, my heart is as pure as the fresh-fallen snow." then you're more dangerous than a rabid canine.

Do you have teeth?

You should know that you have teeth.

Do you have teeth?
