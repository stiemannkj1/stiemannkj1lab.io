---
layout: post
title: The Mediocre Dinosaur (Spoilers!)
permalink: /the-mediocre-dinosaur/
tags: [the-good-dinosaur, movie, review, critique]
excerpt_separator: <!--end-excerpt-->
comments_issue_number: 5
---

*The Good Dinosaur* is a western-style film set in the prehistoric era. The film
opens with an introduction to a family of long-necked Apatosauruses. The titular
main character is Arlo, the easily-frightened runt of the family. Lovingly,
Arlo's father attempts to toughen Arlo up and tasks him with stopping whatever
is stealing the family's corn. Arlo successfully catches the human boy who is
taking the corn, but he cannot bring himself to kill the cave-boy. Instead, Arlo
releases the child. Berating Arlo for freeing the boy, Arlo's father takes Arlo
and sets out to recapture the child. Unfortunately, a flash flood occurs, and
the raging waters drown Arlo's father. <!--end-excerpt--> Grief-stricken, Arlo
returns home. Many days later, he notices that the same boy is stealing his
family's corn again. He chases the boy into the wilderness and gets lost. After
Arlo struggles on his own for a day, the boy begins to help him, and Arlo names
the child "Spot." As Arlo looks for a way home, a trio of pterodactyls attack
him and Spot. Fleeing the pterodactyls, Arlo and Spot run into friendly T. rex
ranchers who chase away the winged villains. Then, the T. rexes ask Arlo to help
them recapture their stolen cattle. Fighting his cowardly nature, Arlo aids the
T. rexes. Together, they chase off the cattle-rustling raptors and recapture the
cattle. After a night's rest, the T. rexes point Arlo towards home. Suddenly,
Arlo and Spot are once more assaulted by the pterodactyls. A newly emboldened
Arlo faces his fears and fights off the attackers. Finally, Arlo unites Spot
with another human family and returns home.

By demonstrating Arlo's confrontations with his own fears, *The Good Dinosaur*
challenges viewers to confront their fears. Arlo learns this moral slowly and
relatably throughout the film. In the beginning, he is cowardly and can barely
do simple (and not-so-scary) tasks like rounding up chickens. But he is forced
to face frightening situations when he is lost and on his own. Eventually, he
helps the T. rexes recapture their cattle from the rustlers and protects Spot
from pterodactlys. The film even contrasts the right understanding of the moral
with the almost-right belief that one should never be afraid at all. Butch, the
oldest T. rex, explains to Arlo that he has been in terrifying life-threatening
situations, but he fought his fears instead of denying them. Unfortunately, the
film directly tells the audience this concept rather than <a
href="https://en.wikipedia.org/wiki/Show,_don%27t_tell" target="_blank">showing
an example</a> of it. Nonetheless, the movie shows clear examples of characters
fighting their fears and encourages the audience to confront their own fears
through those examples.

*The Good Dinosaur* takes advantage of its pre-historic western style by
providing unique and creative dinosaur versions of classic western characters.
Vicious raptor cattle-rustlers and hardened T. rex cowboys are fun versions of
western tropes. Similarly, the villainous storm-worshipping pterodactyls are
unique and memorable. Although the characters are based on tropes, they don't
feel clichéd or lack depth. Since Arlo is the protagonist, he and his family are
simple and relatable farmers. In contrast to the naïve and cowardly Arlo, the T.
rexes are wise and brave. When Arlo interacts with the T. rexes, he learns about
bravery and grows as a character. The storm-obsessed pterodactyls also provide
Arlo with the chance to face his fears and become braver. Most of the characters
have distinct and clear purposes and personalities and help move Arlo along on
his journey.

Although the dinosaur/western style makes the characters feel fun and unique, it
causes issues within the logic of the movie. *The Good Dinosaur* would have made
more sense as a live-action (or animated) western with human beings instead of
dinosaur characters. Ironically, the only human character in film acts like a
dog for no reason. Why was Spot not a normal dog? Similarly, most of the
characters' actions are in direct conflict with the nature of dinosaurs. For
example, the T. rexes don't eat the other dinosaurs, and Arlo's family uses
completely impractical tools to farm their land. Contrast these
anthropomorphized *farming* and *ranching* dinosaurs with characters in another
Pixar movie like *Cars*, and the problem becomes clear. Since *Cars* is about
racing, the characters are anthropomorphized cars. Likewise, since *The
Incredibles* is about superheroes, the characters are super humans. Since *The
Good Dinosaur* is a western about facing fear, the characters are... dinosaurs?

The dinosaur/western idea is confusing, but the movie's greatest sin is its
lengthiness and boring pacing. Many of the scenes go on for far longer than they
need to. Does the audience really need to see that Arlo is afraid of chickens
three times? There is also a completely pointless scene in which a weird
camouflaged dinosaur, who never appears again, tells Arlo to name Spot. Arlo
could have named spot in a two-second scene instead. The ending is also
protracted. Once Arlo has faced his fears and defeated the pterodactyls, the
climax of the film has occurred, so the loose ends should be quickly tied up.
Instead, the movie drags out this section. Drawing out the scene in which Spot
finds a new family, the movie refuses to leave the audience wanting more and
instead makes viewers want less.

In addition to the dinosaur/western mismatch and the dragged-out or unnecessary
scenes, *The Good Dinosaur* has many other minor issues. In the beginning, the
live-action stock footage padded the movie's length and felt distracting and out
of place. In another scene, Arlo falls from a gigantic height and is uninjured.
Even in this animated film, Arlo's lack of injury feels unrealistic. The use of
violence also felt overdone and out of place. Did the simple kind-hearted Arlo
need to savagely and vengefully murder a pterodactyl that was trying to run
away? The humor was disappointing too. Unlike *Finding Nemo* or *Inside Out*,
the jokes seemed to be aimed at children instead of everyone. In spite of the
quality narrative, colorful characters, and good moral, *The Good Dinosaur* is
plagued by the strange dinosaur western style, elongated and pointless scenes,
and plenty of minor issues. *The Mediocre Dinosaur* would have been a more
honest title.

