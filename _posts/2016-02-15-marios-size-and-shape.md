---
layout: post
title: The Brilliance of Mario's Size and Shape
permalink: /marios-size-and-shape/
tags: [mario, design, game-design, video-games, video-game]
comments_issue_number: 3
preview_image_url: "/images/marios-size-and-shape/super-mario.gif"
---

The Super Mario series is amazing---<a
href="http://www.nintendo.co.jp/ir/en/sales/software/wii.html"
target="_blank">every game is a best-seller</a> and a classic. Each entry, even
the original *Super Mario Bros.*, is still fun today. Much of Mario's
timelessness can be attributed to good design, especially the design of Mario
himself.

Here's Mario's normal sprite:

<img alt="Small Mario" src="/images/marios-size-and-shape/small-mario.gif"
width="65" />

Mario's shape is simple. He has sharp edges. He's almost a square (13 by 16
pixels). Because of his square-like stature, it's easy to tell where he is and
where he isn't. When Mario is about to collide with an enemy or block, the
player can plainly see where the collision will occur. When Mario is not
positioned correctly to land on a platform, the clarity of Mario's edges and
location allow the player to finesse Mario into a better spot. The clearness and
simplicity of Mario's size and shape make the game feel fair.

When Mario grabs a mushroom, he turns into Super Mario and his sprite changes
size:

<img alt="Super Mario" src="/images/marios-size-and-shape/super-mario.gif"
width="65" />

Unlike normal Mario, when Super Mario gets hit, he does not die. Instead, he
reverts back to Mario's normal size. Super Mario's designers indicated that
newfound strength visually by doubling Mario's height (and slightly increasing
his width). Super Mario is a towering 32 pixels high, and his width is 16
pixels. This change in size conveys Super Mario's increase in strength over
normal Mario without cluttering the screen with information like hearts or a
life bar. The player can tell exactly how strong Mario is just by looking at
him. The strength conveyance is brilliant enough, but Super Mario's doubled
stature also helps to balance the game. Since he is more than double the size of
normal Mario, Super Mario can be hit by enemies twice as easily. Some obstacles
just soar over normal Mario's head, but Super Mario must dodge them. Playing as
Super Mario adds a new level of challenge and getting hit and turning back into
normal Mario makes the game a little easier even though the stakes are higher.

The design of Mario's size and shape and the contrast between normal Mario and
Super Mario showcase the brilliance of Mario's developers. They created Mario's
simple square design to clearly communicate danger and make collisions feel
fair. And they used the player's avatar to convey strength and keep the
difficulty of the game consistent whether the player is normal Mario or Super
Mario. Mario's size and shape are only a tiny aspect of *Super Mario Bros.*
brilliant game design, but that kind of thoughtful, intentional design has made
the Super Mario series a bestselling series and kept Mario relevant for 30
years.

<sub><sup>Mario's sprites are copyright Nintendo and are published here under
fair use for educational purposes.</sup></sub>

