---
layout: post
title: Programming Terms Cheat Sheet
permalink: /programming-terms-cheat-sheet/
tags: [computer-science, programming, software, software-engineering, coding]
comments_issue_number: 13
---

This is a cheat sheet showing various programming terms alongside example code (in Java) of what the terms describe. I created this document to give junior developers and interns a reference for the terms that developers often use to describe different elements of a program. The goal of this document is to allow senior developers to use technical language and be somewhat Socratic during code reviews with new programmers instead of pointing to specific lines and explaining exactly what to do. If a junior developer's code is being reviewed and he or she is asked, "Why did you pass the `obtainClass()` method a `String` variable?", he or she can quickly search this sheet for the unknown terms and try to answer the question. It will also help new developers to know what parts of the code senior devs are talking about---for example: `obtainClass()`'s method signature versus an `obtainClass()` method call. Rather than create an exhaustive reference, I want this cheat sheet to be brief, so I only included the technical words that I use the most during my current code reviews. However, I'm sure this document is not perfect, so if you would like to add or fix something, please <a href="https://gitlab.com/stiemannkj1/stiemannkj1.gitlab.io/issues/{{ page.comments_issue_number }}" target="_blank">write a comment</a> explaining what could be improved and/or make the improvement yourself and <a href="https://gitlab.com/stiemannkj1/stiemannkj1.gitlab.io/edit/master/_posts/2016-12-26-programming-terms-cheat-sheet.md" target="_blank">send me a pull request</a>.

Note that <a id="underlinedText" href="#underlinedText">underlined text</a> can be clicked to navigate to another section of the cheat sheet which shows terms for each element of that line of code.

<table class="code-columns">
    <thead>
        <th><code class="comment">/* Term */</code></th>
        <th><code>Example Code</code></th>
    </thead>
    <tbody>
        <tr>
            <td>
                <pre><code class="comment">
/* Class Declaration: */

<a id="stringBack" href="#string">/* Constant Declaration: */</a>

<a id="stringBack" href="#string">/* Member Variable Declaration: */</a>

<a id="methodSignatureBack" href="#methodSignature">/* Method Signature: */</a>
/* Method Body (begin): */

/* Conditional (If) Statement: */
/* If Block (begin): */

/* Return Statement: */

/* If Block (end): */
/* Else Block (begin): */



/* Else Block (end): */

/* Method Body (end): */



<a id="localVariableDeclarationBack" href="#localVariableDeclaration">/* Local Variable Declaration: */</a>

/* Variable Assignment: */



                </code></pre>
            </td>
            <td>
                <pre><code>
public class ExampleClass {

    <a href="#string">private static final String PREFIX = "prefix";</a>

    <a href="#string">String value;</a>

    <a href="#methodSignature">public String getValue(boolean prefixValue)</a>
    {

        if ( <span class="comment">/* Condition: */</span> prefixValue)
        {

            return PREFIX + <span class="comment">/* Member Variable: */</span> value;

        }
        else {

            return value;

        }

    }

    public void runTest() {

        <a href="#localVariableDeclaration">boolean valueStartsWithPrefix = getValue(false).startsWith(PREFIX);</a>
        Assert.assertFalse(valueStartsWithPrefix);
        valueStartsWithPrefix = getValue(true).startsWith(PREFIX);
        Assert.assertTrue(valueStartsWithPrefix);
    }
}
                </code></pre>
            </td>
        </tr>
        <tr id="string">
            <th colspan="2"><code class="comment"><a href="#stringBack">/* Types of Strings */</a></code></th>
        </tr>
        <tr>
            <td>
                <pre><code class="comment">






/* String Constant: */

/* String Variable: */

/* String Literal: */
                </code></pre>
            </td>
            <td>
                <pre><code>
private static final String PREFIX = "prefix";

String value;

<span class="comment">/* ... */</span>

PREFIX

value

"prefix"
                </code></pre>
            </td>
        </tr>
        <tr id="methodSignature">
            <th colspan="2"><code class="comment"><a href="#methodSignatureBack">/* Elements of a Method Signature */</a></code></th>
        </tr>
        <tr>
            <td>
                <pre><code class="comment">
/* Access Level Modifier: */
/* Return Type: */
/* Method Name: */

/* Method Parameter: */


                </code></pre>
            </td>
            <td>
                <pre><code>
public
String
getValue
(
boolean prefixValue
)
{
                </code></pre>
            </td>
        </tr>
        <tr id="localVariableDeclaration">
            <th colspan="2"><code class="comment"><a href="#localVariableDeclarationBack">/* Elements of a Local Variable Declaration Statement */</a></code></th>
        </tr>
        <tr>
            <td>
                <pre><code class="comment">
/* Variable Type: */
/* Variable Name: */
/* Assignment Operator: */
/* Method Call: */
/* Value Passed to Method: */

                </code></pre>
            </td>
            <td>
                <pre><code>
boolean
valueStartsWithPrefix
=
getValue(
false
).startsWith(PREFIX);
                </code></pre>
            </td>
        </tr>
    </tbody>
</table>