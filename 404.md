---
layout: center
permalink: /404.html
---

# 404

Sorry, I can't seem to find the page you are looking for.

<div class="mt3">
  <a href="{{ site.baseurl }}/" class="button button-blue button-big">Home</a>
  <a href="{{ site.baseurl }}/about/" class="button button-blue button-big">About</a>
</div>
